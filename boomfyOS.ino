#include "Arduino.h"
#include "tasks/IdleTask.h"
#include "tasks/LEDTask.h"
#include "tasks/AudioTask.h"
#include "tasks/BluetoothTask.h"
#include "tasks/WifiTask.h"
#include "tasks/ButtonTask.h"
//The setup function is called once at startup of the sketch
void setup()
{
	Serial.begin(115200);
	static IdleTask& idleTask = IdleTask::getInstance();
	static AudioTask& audioTask = AudioTask::getInstance();
	static BluetoothTask& btTask = BluetoothTask::getInstance();
	static WifiTask& wifiTask = WifiTask::getInstance();
	static ButtonTask& btnTask = ButtonTask::getInstance();
}

// The loop function is called in an endless loop
void loop()
{
	static IdleTask& idleTask = IdleTask::getInstance();
	static LEDTask& ledTask = LEDTask::getInstance();
	IdleTask::idleMessage imsg;
	LEDTask::ledMessage lmsg;
	imsg.event = IdleTask::IDLE;
	lmsg.id = LEDTask::LED_POWER;
	lmsg.rgb = rand();
	xQueueSend(idleTask.queue,&imsg,QUEUE_MAX_WAIT_MS);
	xQueueSend(ledTask.queue,&lmsg,QUEUE_MAX_WAIT_MS);
	delay(500);
}
