/*
 * PLATFORM.h
 *
 *  Created on: 20.03.2019
 *      Author: pmale
 */

#ifndef PLATFORM_H_
#define PLATFORM_H_

#include "Arduino.h"

#define MS_TO_TICKS (1/portTICK_PERIOD_MS)

#define STACK_DEPTH 2048
#define QUEUE_SIZE 100
#define PRIO_IDLE_TASK 0
#define QUEUE_MAX_WAIT_MS 100*MS_TO_TICKS

#define UNUSED(x) (void)(x)

#endif /* PLATFORM_H_ */
