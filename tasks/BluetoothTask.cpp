/*
 * BluetoothTask.cpp
 *
 *  Created on: 03.09.2019
 *      Author: pmale
 */

#include "BluetoothTask.h"
#include "driver/i2s.h"

/* event for handler "bt_av_hdl_stack_up */
enum {
    BT_APP_EVT_STACK_UP = 0,
};

void a2dpCallback(esp_a2d_cb_event_t event, esp_a2d_cb_param_t *param);
void a2dpDataCallback(const uint8_t *data, uint32_t len);
void avrcpControllerCallback(esp_avrc_ct_cb_event_t event, esp_avrc_ct_cb_param_t *param);
bool sendWorkMessage(BluetoothTask::msgType type, uint16_t event, void *data, uint32_t dataSize);
void gapCallback(esp_bt_gap_cb_event_t event, esp_bt_gap_cb_param_t *param);

uint32_t BluetoothTask::s_pkt_cnt = 0;
esp_a2d_audio_state_t BluetoothTask::s_audio_state = ESP_A2D_AUDIO_STATE_STOPPED;
BluetoothTask::btState BluetoothTask::state = BluetoothTask::STATE_OFF;
esp_bd_addr_t BluetoothTask::remote_addr = {0};

BluetoothTask& BluetoothTask::getInstance()
{
	static BluetoothTask _instance;
	return _instance;
}

BluetoothTask::BluetoothTask()
{
	esp_err_t err;
	queue = xQueueCreate(QUEUE_SIZE, sizeof(btMessage));
	btStart();

	if ((err = esp_bluedroid_init()) != ESP_OK) {
		ESP_LOGE(BT_AV_TAG, "%s initialize bluedroid failed: %s\n", __func__, esp_err_to_name(err));
		return;
	}

	if ((err = esp_bluedroid_enable()) != ESP_OK) {
		ESP_LOGE(BT_AV_TAG, "%s enable bluedroid failed: %s\n", __func__, esp_err_to_name(err));
		return;
	}

	handleStackEvent(BT_APP_EVT_STACK_UP,NULL);

	/* Set default parameters for Secure Simple Pairing */
	esp_bt_sp_param_t param_type = ESP_BT_SP_IOCAP_MODE;
	esp_bt_io_cap_t iocap = ESP_BT_IO_CAP_IO;
	esp_bt_gap_set_security_param(param_type, &iocap, sizeof(uint8_t));

	/*
	 * Set default parameters for Legacy Pairing
	 * Use fixed pin code
	 */
	esp_bt_pin_type_t pin_type = ESP_BT_PIN_TYPE_FIXED;
	esp_bt_pin_code_t pin_code;
	pin_code[0] = '0';
	pin_code[1] = '0';
	pin_code[2] = '0';
	pin_code[3] = '0';
	esp_bt_gap_set_pin(pin_type, 4, pin_code);

	xTaskCreate(runWrapper, "btTask", STACK_DEPTH, this, PRIO_IDLE_TASK, &task);
}

void BluetoothTask::handleStackEvent(uint16_t event, void *p_param)
{
    ESP_LOGD(BT_AV_TAG, "%s evt %d", __func__, event);
    switch (event) {
    case BT_APP_EVT_STACK_UP: {
        /* set up device name */
        char *dev_name = "BoomFy";
        esp_bt_dev_set_device_name(dev_name);

        esp_bt_gap_register_callback(gapCallback);
        /* initialize A2DP sink */
        esp_a2d_register_callback(&a2dpCallback);
        esp_a2d_sink_register_data_callback(a2dpDataCallback);
        esp_a2d_sink_init();

        /* initialize AVRCP controller */
        esp_avrc_ct_init();
        esp_avrc_ct_register_callback(avrcpControllerCallback);

        /* set discoverable and connectable mode, wait to be connected */
        esp_bt_gap_set_scan_mode(ESP_BT_SCAN_MODE_CONNECTABLE_DISCOVERABLE);
        state = STATE_DISCONNECTED;
        memset(remote_addr,0,sizeof(remote_addr));	//init with 0
        break;
    }
    default:
        ESP_LOGE(BT_AV_TAG, "%s unhandled evt %d", __func__, event);
        break;
    }
}

BluetoothTask::btState BluetoothTask::getState()
{
	return state;
}

BluetoothTask::~BluetoothTask()
{

}

void BluetoothTask::run()
{

    btMessage msg;
    for (;;) {
        if (pdTRUE == xQueueReceive(queue, &msg, (portTickType)portMAX_DELAY)) {
            ESP_LOGD(BT_APP_CORE_TAG, "%s, sig 0x%x, 0x%x", __func__, msg.sig, msg.event);


            switch(msg.type){
            	case WORK_HANDLE_STACK_EVENT:
            		handleStackEvent(msg.event,msg.data);
            		break;
            	case WORK_HANDLE_A2DP_EVENT:
            		handleA2DPEvent(msg.event, msg.data);
            		break;
            	case WORK_HANDLE_AVRCP_EVENT:
            		handleAVRCEvent(msg.event, msg.data);
            		break;
            	case BUTTON_EVENT:
            		if(remote_addr[0] != 0)
            		{
            			esp_a2d_sink_disconnect(remote_addr);	//disconnect device
            		}
            		break;
            }

            if (msg.dataSize) {
                free(msg.data);
            }

        }
    }

}

void BluetoothTask::setI2SClock(uint32_t samplerate)
{
	static AudioTask& audioTask = AudioTask::getInstance();
	AudioTask::audioMessage msg;
	msg.event = AudioTask::MSG_SET_CLK;
	msg.data = (void *) samplerate;
	msg.data = 0;
    xQueueSend(audioTask.queue,&msg,portMAX_DELAY);
}

void BluetoothTask::handleA2DPEvent(uint16_t event, void *p_param)
{
    ESP_LOGD(BT_AV_TAG, "%s evt %d", __func__, event);
    esp_a2d_cb_param_t *a2d = NULL;
    switch (event) {
    case ESP_A2D_CONNECTION_STATE_EVT: {
        a2d = (esp_a2d_cb_param_t *)(p_param);
        memcpy(remote_addr,a2d->conn_stat.remote_bda,sizeof(remote_addr));
        ESP_LOGI(BT_AV_TAG, "A2DP connection state: %s, [%02x:%02x:%02x:%02x:%02x:%02x]",
             s_a2d_conn_state_str[a2d->conn_stat.state], remote_addr[0], remote_addr[1], remote_addr[2], remote_addr[3], remote_addr[4], remote_addr[5]);
        if (a2d->conn_stat.state == ESP_A2D_CONNECTION_STATE_DISCONNECTED) {
            esp_bt_gap_set_scan_mode(ESP_BT_SCAN_MODE_CONNECTABLE_DISCOVERABLE);
            state = STATE_DISCONNECTED;
            memset(remote_addr,0,sizeof(remote_addr));
        } else if (a2d->conn_stat.state == ESP_A2D_CONNECTION_STATE_CONNECTED){
            esp_bt_gap_set_scan_mode(ESP_BT_SCAN_MODE_NONE);
            state = STATE_CONNECTED;
        }
        break;
    }
    case ESP_A2D_AUDIO_STATE_EVT: {
        a2d = (esp_a2d_cb_param_t *)(p_param);
        ESP_LOGI(BT_AV_TAG, "A2DP audio state: %s", s_a2d_audio_state_str[a2d->audio_stat.state]);
        s_audio_state = a2d->audio_stat.state;
        if (ESP_A2D_AUDIO_STATE_STARTED == a2d->audio_stat.state) {
            s_pkt_cnt = 0;
        }
        break;
    }
    case ESP_A2D_AUDIO_CFG_EVT: {
        a2d = (esp_a2d_cb_param_t *)(p_param);
        ESP_LOGI(BT_AV_TAG, "A2DP audio stream configuration, codec type %d", a2d->audio_cfg.mcc.type);
        // for now only SBC stream is supported
        if (a2d->audio_cfg.mcc.type == ESP_A2D_MCT_SBC) {
            int sample_rate = 16000;
            char oct0 = a2d->audio_cfg.mcc.cie.sbc[0];
            if (oct0 & (0x01 << 6)) {
                sample_rate = 32000;
            } else if (oct0 & (0x01 << 5)) {
                sample_rate = 44100;
            } else if (oct0 & (0x01 << 4)) {
                sample_rate = 48000;
            }
            setI2SClock(sample_rate);

            ESP_LOGI(BT_AV_TAG, "Configure audio player %x-%x-%x-%x",
                     a2d->audio_cfg.mcc.cie.sbc[0],
                     a2d->audio_cfg.mcc.cie.sbc[1],
                     a2d->audio_cfg.mcc.cie.sbc[2],
                     a2d->audio_cfg.mcc.cie.sbc[3]);
            ESP_LOGI(BT_AV_TAG, "Audio player configured, sample rate=%d", sample_rate);
        }
        break;
    }
    default:
        ESP_LOGE(BT_AV_TAG, "%s unhandled evt %d", __func__, event);
        break;
    }
}

void BluetoothTask::handleAVRCEvent(uint16_t event, void *p_param)
{
    ESP_LOGD(BT_AV_TAG, "%s evt %d", __func__, event);
    esp_avrc_ct_cb_param_t *rc = (esp_avrc_ct_cb_param_t *)(p_param);
    switch (event) {
    case ESP_AVRC_CT_CONNECTION_STATE_EVT: {
    	memcpy(remote_addr,rc->conn_stat.remote_bda,sizeof(remote_addr));
        ESP_LOGI(BT_AV_TAG, "AVRC conn_state evt: state %d, [%02x:%02x:%02x:%02x:%02x:%02x]",
                 rc->conn_stat.connected, remote_addr[0], remote_addr[1], remote_addr[2], remote_addr[3], remote_addr[4], remote_addr[5]);

        if (rc->conn_stat.connected) {
            //Register notifications and request metadata
            esp_avrc_ct_send_metadata_cmd(0, ESP_AVRC_MD_ATTR_TITLE | ESP_AVRC_MD_ATTR_ARTIST | ESP_AVRC_MD_ATTR_ALBUM | ESP_AVRC_MD_ATTR_GENRE);
            esp_avrc_ct_send_register_notification_cmd(1, ESP_AVRC_RN_TRACK_CHANGE, 0);
        }
        break;
    }
    case ESP_AVRC_CT_PASSTHROUGH_RSP_EVT: {
        ESP_LOGI(BT_AV_TAG, "AVRC passthrough rsp: key_code 0x%x, key_state %d", rc->psth_rsp.key_code, rc->psth_rsp.key_state);
        break;
    }
    case ESP_AVRC_CT_METADATA_RSP_EVT: {
        ESP_LOGI(BT_AV_TAG, "AVRC metadata rsp: attribute id 0x%x, %s", rc->meta_rsp.attr_id, rc->meta_rsp.attr_text);
        free(rc->meta_rsp.attr_text);
        break;
    }
    case ESP_AVRC_CT_CHANGE_NOTIFY_EVT: {
        ESP_LOGI(BT_AV_TAG, "AVRC event notification: %d, param: %d", rc->change_ntf.event_id, rc->change_ntf.event_parameter);
        switch (rc->change_ntf.event_id) {
        case ESP_AVRC_RN_TRACK_CHANGE:
            //Register notifications and request metadata
            esp_avrc_ct_send_metadata_cmd(0, ESP_AVRC_MD_ATTR_TITLE | ESP_AVRC_MD_ATTR_ARTIST | ESP_AVRC_MD_ATTR_ALBUM | ESP_AVRC_MD_ATTR_GENRE);
            esp_avrc_ct_send_register_notification_cmd(1, ESP_AVRC_RN_TRACK_CHANGE, 0);
            break;
        }
        break;
    }
    case ESP_AVRC_CT_REMOTE_FEATURES_EVT: {
        ESP_LOGI(BT_AV_TAG, "AVRC remote features %x", rc->rmt_feats.feat_mask);
        break;
    }
    default:
        ESP_LOGE(BT_AV_TAG, "%s unhandled evt %d", __func__, event);
        break;
    }
}

bool sendWorkMessage(BluetoothTask::msgType type, uint16_t event, void *data, uint32_t dataSize)
{
	static BluetoothTask &btTask = BluetoothTask::getInstance();
    ESP_LOGD(BT_APP_CORE_TAG, "%s event 0x%x, param len %d", __func__, event, param_len);

    BluetoothTask::btMessage msg;
    memset(&msg, 0, sizeof(BluetoothTask::btMessage));

    msg.type = type;
    msg.event = event;

    if (dataSize == 0) {
        return (xQueueSend(btTask.queue, &msg, 10 / portTICK_RATE_MS) == pdTRUE);
    } else if (data && dataSize > 0) {
        if ((msg.data = malloc(dataSize)) != NULL) {
            memcpy(msg.data, data, dataSize);
            return (xQueueSend(btTask.queue, &msg, 10 / portTICK_RATE_MS) == pdTRUE);
        }
    }
    return false;
}

void gapCallback(esp_bt_gap_cb_event_t event, esp_bt_gap_cb_param_t *param)
{
    switch (event) {
    case ESP_BT_GAP_AUTH_CMPL_EVT: {
        if (param->auth_cmpl.stat == ESP_BT_STATUS_SUCCESS) {
            ESP_LOGI(BT_AV_TAG, "authentication success: %s", param->auth_cmpl.device_name);
            //esp_log_buffer_hex(BT_AV_TAG, param->auth_cmpl.bda, ESP_BD_ADDR_LEN);
        } else {
            ESP_LOGE(BT_AV_TAG, "authentication failed, status:%d", param->auth_cmpl.stat);
        }
        break;
    }
    case ESP_BT_GAP_CFM_REQ_EVT:
        ESP_LOGI(BT_AV_TAG, "ESP_BT_GAP_CFM_REQ_EVT Please compare the numeric value: %d", param->cfm_req.num_val);
        esp_bt_gap_ssp_confirm_reply(param->cfm_req.bda, true);
        break;
    case ESP_BT_GAP_KEY_NOTIF_EVT:
        ESP_LOGI(BT_AV_TAG, "ESP_BT_GAP_KEY_NOTIF_EVT passkey:%d", param->key_notif.passkey);
        break;
    case ESP_BT_GAP_KEY_REQ_EVT:
        ESP_LOGI(BT_AV_TAG, "ESP_BT_GAP_KEY_REQ_EVT Please enter passkey!");
        break;
    default: {
        ESP_LOGI(BT_AV_TAG, "event: %d", event);
        break;
    }
    }
    return;
}

/* callback for A2DP sink */
void a2dpCallback(esp_a2d_cb_event_t event, esp_a2d_cb_param_t *param)
{
    switch (event) {
    case ESP_A2D_CONNECTION_STATE_EVT:
    case ESP_A2D_AUDIO_STATE_EVT:
    case ESP_A2D_AUDIO_CFG_EVT: {
        sendWorkMessage(BluetoothTask::WORK_HANDLE_A2DP_EVENT, event, param, sizeof(esp_a2d_cb_param_t));
        break;
    }
    default:
        ESP_LOGE(BT_AV_TAG, "Invalid A2DP event: %d", event);
        break;
    }
}

void a2dpDataCallback(const uint8_t *data, uint32_t len)
{
	static AudioTask &audioTask = AudioTask::getInstance();
	audioTask.write(data, len);
}

void allocateMetaBuffer(esp_avrc_ct_cb_param_t *param)
{
    esp_avrc_ct_cb_param_t *rc = (esp_avrc_ct_cb_param_t *)(param);
    uint8_t *attr_text = (uint8_t *) malloc (rc->meta_rsp.attr_length + 1);
    memcpy(attr_text, rc->meta_rsp.attr_text, rc->meta_rsp.attr_length);
    attr_text[rc->meta_rsp.attr_length] = 0;

    rc->meta_rsp.attr_text = attr_text;
}

void avrcpControllerCallback(esp_avrc_ct_cb_event_t event, esp_avrc_ct_cb_param_t *param)
{
    switch (event) {
    case ESP_AVRC_CT_METADATA_RSP_EVT:
        allocateMetaBuffer(param);
        /* fall through */
    case ESP_AVRC_CT_CONNECTION_STATE_EVT:
    case ESP_AVRC_CT_PASSTHROUGH_RSP_EVT:
    case ESP_AVRC_CT_CHANGE_NOTIFY_EVT:
    case ESP_AVRC_CT_REMOTE_FEATURES_EVT: {
        sendWorkMessage(BluetoothTask::WORK_HANDLE_AVRCP_EVENT, event, param, sizeof(esp_avrc_ct_cb_param_t));
        break;
    }
    default:
        ESP_LOGE(BT_AV_TAG, "Invalid AVRC event: %d", event);
        break;
    }
}
