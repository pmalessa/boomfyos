/*
 * WifiTask.cpp
 *
 *  Created on: 03.09.2019
 *      Author: pmale
 */

#include "WifiTask.h"

WifiTask& WifiTask::getInstance()
{
	static WifiTask _instance;
	return _instance;
}

WifiTask::WifiTask()
{
	queue = xQueueCreate(QUEUE_SIZE, sizeof(wifiMessage));
	xTaskCreate(runWrapper, "WifiTask", STACK_DEPTH, this, PRIO_IDLE_TASK, &task);

	WiFi.softAP(ssid);
	IPAddress myIP = WiFi.softAPIP();
	Serial.print("AP IP address: ");
	Serial.println(myIP);
}

WifiTask::~WifiTask()
{

}


void WifiTask::run()
{
	wifiMessage msg;
	while(1)
	{

		if(xQueueReceive(queue,&msg,QUEUE_MAX_WAIT_MS))
		{
			//handle received message
		}
		//sleep if nothing to do
	}
}
