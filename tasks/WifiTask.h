/*
 * WifiTask.h
 *
 *  Created on: 03.09.2019
 *      Author: pmale
 */

#ifndef TASKS_WIFITASK_H_
#define TASKS_WIFITASK_H_

#include "../PLATFORM.h"
#include "../templates/BoomfyTask.h"
#include <WiFi.h>
#include <WiFiClient.h>
#include <WiFiAP.h>

class WifiTask : public BoomfyTask{
public:
	typedef enum{
		IDLE,
		BUTTON_EVENT
	}wifiEvent;
	typedef struct{
		wifiEvent event;
		void *data;
		uint32_t dataSize;
	}wifiMessage;

	static WifiTask& getInstance();
	~WifiTask();
private:
	WifiTask();
	WifiTask(const WifiTask&);
	WifiTask & operator = (const WifiTask &);
	void run();

	const char *ssid = "BoomFy";
	const char *password = "boomfy";
};


#endif /* TASKS_IDLETASK_H_ */
