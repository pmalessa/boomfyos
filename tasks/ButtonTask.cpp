/*
 * ButtonTask.cpp
 *
 *  Created on: 03.09.2019
 *      Author: pmale
 */

#include "ButtonTask.h"

ButtonTask& ButtonTask::getInstance()
{
	static ButtonTask _instance;
	return _instance;
}

ButtonTask::ButtonTask()
{
	bt_pressed = false;
	wifi_pressed = false;
	xTaskCreate(runWrapper, "ButtonTask", STACK_DEPTH, this, PRIO_IDLE_TASK, &task);

	attachInterruptArg(BUT_BT, btisrWrapper,this, CHANGE);
	attachInterruptArg(BUT_WIFI, wifiisrWrapper,this, CHANGE);
}

ButtonTask::~ButtonTask()
{

}

//called when button changes
void ButtonTask::bt_isr()
{
	if(digitalRead(BUT_BT) == LOW)
	{
		bt_pressed = true;
	}
}

//called when button changes
void ButtonTask::wifi_isr()
{
	if(digitalRead(BUT_WIFI) == LOW)
	{
		wifi_pressed = true;
	}
}

void ButtonTask::run()
{
	static BluetoothTask& btTask = BluetoothTask::getInstance();
	static WifiTask& wifiTask = WifiTask::getInstance();
	static WifiTask::wifiMessage w_msg;
	static BluetoothTask::btMessage b_msg;
	w_msg.event = WifiTask::BUTTON_EVENT;
	b_msg.type = BluetoothTask::BUTTON_EVENT;
	while(1)
	{
		if(bt_pressed)
		{
			bt_pressed = false;
			xQueueSend(btTask.queue,&b_msg,10*MS_TO_TICKS);
		}
		if(wifi_pressed)
		{
			wifi_pressed = false;
			xQueueSend(wifiTask.queue,&w_msg,10*MS_TO_TICKS);
		}
		vTaskDelay(100*MS_TO_TICKS);
	}
}
