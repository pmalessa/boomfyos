/*
 * BluetoothTask.h
 *
 *  Created on: 03.09.2019
 *      Author: pmale
 */

#ifndef TASKS_BLUETOOTHTASK_H_
#define TASKS_BLUETOOTHTASK_H_

#include "../PLATFORM.h"
#include "../templates/BoomfyTask.h"
#include "AudioTask.h"
#include "esp_bt_main.h"
#include "esp_bt_device.h"
#include "esp_gap_bt_api.h"
#include "esp_a2dp_api.h"
#include "esp_avrc_api.h"

#define BT_APP_SIG_WORK_DISPATCH          (0x01)

class BluetoothTask : public BoomfyTask{
public:
	typedef enum{
		WORK_HANDLE_STACK_EVENT,
		WORK_HANDLE_A2DP_EVENT,
		WORK_HANDLE_AVRCP_EVENT,
		BUTTON_EVENT
	}msgType;
	typedef struct{
		msgType type;
	    uint16_t event;    /*!< message event id */
		uint32_t dataSize;
		void *data;		//needs to be last
	}btMessage;
	typedef enum{
		STATE_CONNECTED,
		STATE_DISCONNECTED,
		STATE_OFF
	}btState;

	btState getState();
	static BluetoothTask& getInstance();
	~BluetoothTask();
private:
	BluetoothTask();
	BluetoothTask(const BluetoothTask&);
	BluetoothTask & operator = (const BluetoothTask &);
	void run();

	static void handleStackEvent(uint16_t event, void *p_param);
	static void handleA2DPEvent(uint16_t event, void *p_param);	/* a2dp event handler */
	static void handleAVRCEvent(uint16_t event, void *p_param);	/* avrc event handler */
	static void setI2SClock(uint32_t samplerate);
	static uint32_t s_pkt_cnt;
	static esp_a2d_audio_state_t s_audio_state;
	static btState state;
	static esp_bd_addr_t remote_addr;
};


#endif /* TASKS_BLUETOOTHTASK_H_ */
