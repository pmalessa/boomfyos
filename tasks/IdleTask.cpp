/*
 * IdleTask.cpp
 *
 *  Created on: 03.09.2019
 *      Author: pmale
 */

#include "IdleTask.h"

IdleTask& IdleTask::getInstance()
{
	static IdleTask _instance;
	return _instance;
}

IdleTask::IdleTask()
{
	queue = xQueueCreate(QUEUE_SIZE, sizeof(idleMessage));
	xTaskCreate(runWrapper, "IdleTask", STACK_DEPTH, this, PRIO_IDLE_TASK, &task);
}

IdleTask::~IdleTask()
{

}


//Idle Task gets executed once no other Task has something to do
void IdleTask::run()
{
	idleMessage msg;
	while(1)
	{

		if(xQueueReceive(queue,&msg,QUEUE_MAX_WAIT_MS))
		{
			//handle received message
			Serial.println(xPortGetFreeHeapSize());
		}
		//sleep if nothing to do
	}
}
