/*
 * LEDTask.h
 *
 *  Created on: 03.09.2019
 *      Author: pmale
 */

#ifndef TASKS_LEDTASK_H_
#define TASKS_LEDTASK_H_

#include "../PLATFORM.h"
#include "../templates/BoomfyTask.h"
#include "FastLED.h"
#include "BluetoothTask.h"

class LEDTask : public BoomfyTask{
public:
	#define FRAMERATE (1/60)*MS_TO_TICKS
	typedef enum{
		LED_POWER,
		LED_STATE,
		LED_BT
	}ledID;
	typedef struct{
		ledID id;
		uint32_t rgb;
	}ledMessage;

	static LEDTask& getInstance();
	~LEDTask();
private:
	void setColor(ledID id, uint32_t rgb);
	LEDTask();
	LEDTask(const LEDTask&);
	LEDTask & operator = (const LEDTask &);
	void run();
	xTaskHandle fadeTask;
};


#endif /* TASKS_LEDTASK_H_ */
