/*
 * LEDTask.cpp
 *
 *  Created on: 03.09.2019
 *      Author: pmale
 */

#include "LEDTask.h"

CRGB leds[3];

LEDTask& LEDTask::getInstance()
{
	static LEDTask _instance;
	return _instance;
}

LEDTask::LEDTask()
{
	FastLED.addLeds<NEOPIXEL,4>(leds, 3);
	FastLED.setBrightness(255);

	queue = xQueueCreate(QUEUE_SIZE, sizeof(ledMessage));
	xTaskCreate(runWrapper, "LEDTask", STACK_DEPTH, this, PRIO_IDLE_TASK, &task);
}

LEDTask::~LEDTask()
{

}


void LEDTask::run()
{
	static BluetoothTask& btTask = BluetoothTask::getInstance();
	ledMessage msg;
	while(1)
	{

		if(xQueueReceive(queue,&msg,0))
		{
			setColor(msg.id, msg.rgb);
		}
		switch (btTask.getState()) {
			case btTask.STATE_OFF:
				setColor(LED_BT, 0x000000);
				break;
			case btTask.STATE_DISCONNECTED:
				setColor(LED_BT, 0x000020);
				break;
			case btTask.STATE_CONNECTED:
				setColor(LED_BT, 0x0000E0);
				break;
			default:
				break;
		}
		FastLED.show();
		vTaskDelay(FRAMERATE);
	}
}

void LEDTask::setColor(ledID id, uint32_t rgb)
{
	leds[id] = CRGB(rgb);
	//leds->setPixelColor(id, (rgb &0xFF0000) >> 16, (rgb &0x00FF00) >> 8, (rgb &0x0000FF) >> 0 );
}
