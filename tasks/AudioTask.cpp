/*
 * AudioTask.cpp
 *
 *  Created on: 03.09.2019
 *      Author: pmale
 */

#include "AudioTask.h"

AudioTask& AudioTask::getInstance()
{
	static AudioTask _instance;
	return _instance;
}

AudioTask::AudioTask()
{
    i2s_mode_t i2s_mode = (i2s_mode_t) (I2S_MODE_MASTER | I2S_MODE_TX);

    i2s_config_t i2s_config = {
        mode : i2s_mode,                                  // Only TX
        sample_rate : 44100,
        bits_per_sample : I2S_BITS_PER_SAMPLE_16BIT,
        channel_format : I2S_CHANNEL_FMT_RIGHT_LEFT,                           //2-channels
        communication_format : I2S_COMM_FORMAT_I2S_MSB,
		intr_alloc_flags : 0,
        dma_buf_count : 6,
        dma_buf_len : 60,
		use_apll : false,
        tx_desc_auto_clear : true,
		fixed_mclk : 0
    };


    i2s_driver_install(I2S_NUM_0, &i2s_config, 0, NULL);
    i2s_pin_config_t pin_config = {
        .bck_io_num = I2S_BCK_PIN,
        .ws_io_num = I2S_LRCK_PIN,
        .data_out_num = I2S_DATA_PIN,
        .data_in_num = -1                                                       //Not used
    };

    i2s_set_pin(I2S_NUM_0, &pin_config);
    setI2SClock(44100);

	queue = xQueueCreate(QUEUE_SIZE, sizeof(audioMessage));
	xTaskCreate(runWrapper, "AudioTask", STACK_DEPTH, this, PRIO_IDLE_TASK, &task);
}

AudioTask::~AudioTask()
{

}


void AudioTask::run()
{
	audioMessage msg;
	while(1)
	{
		if(xQueueReceive(queue,&msg,QUEUE_MAX_WAIT_MS))
		{
			switch (msg.event) {
				case MSG_SET_CLK:
					setI2SClock((uint32_t)msg.data);
					break;
				case MSG_AUDIO_DATA:
					write(msg.data, msg.dataSize);
					break;
				default:
					break;
			}
		}
	}
}

int AudioTask::write(const void *data, unsigned int len)
{
	static uint32_t written;
	return i2s_write(I2S_NUM_0, data, len, &written, portMAX_DELAY);
	Serial.print(len);
	Serial.print(" ");
	Serial.println(written);
}

void AudioTask::setI2SClock(uint32_t samplerate)
{
	i2s_set_clk(I2S_NUM_0, samplerate, I2S_BITS_PER_SAMPLE_16BIT, I2S_CHANNEL_STEREO);
}
