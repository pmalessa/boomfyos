/*
 * AudioTask.h
 *
 *  Created on: 03.09.2019
 *      Author: pmale
 */

#ifndef TASKS_AUDIOTASK_H_
#define TASKS_AUDIOTASK_H_

#include "../PLATFORM.h"
#include "../templates/BoomfyTask.h"
#include "driver/i2s.h"

class AudioTask : public BoomfyTask{
public:
	#define I2S_BCK_PIN 16
	#define I2S_LRCK_PIN 18
	#define I2S_DATA_PIN 17

	typedef enum{
		MSG_SET_CLK,
		MSG_AUDIO_DATA
	}msgType;
	typedef struct{
		msgType event;
		void *data;
		uint32_t dataSize;
	}audioMessage;
	int write(const void *data, unsigned int len);
	static AudioTask& getInstance();
	~AudioTask();
private:
	AudioTask();
	AudioTask(const AudioTask&);
	AudioTask & operator = (const AudioTask &);
	void run();
	void setI2SClock(uint32_t samplerate);

};


#endif /* TASKS_IDLETASK_H_ */
