/*
 * IdleTask.h
 *
 *  Created on: 03.09.2019
 *      Author: pmale
 */

#ifndef TASKS_IDLETASK_H_
#define TASKS_IDLETASK_H_

#include "../PLATFORM.h"
#include "../templates/BoomfyTask.h"

class IdleTask : public BoomfyTask{
public:
	typedef enum{
		IDLE
	}idleEvent;
	typedef struct{
		idleEvent event;
		void *data;
		uint32_t dataSize;
	}idleMessage;

	static IdleTask& getInstance();
	~IdleTask();
private:
	IdleTask();
	IdleTask(const IdleTask&);
	IdleTask & operator = (const IdleTask &);
	void run();
};


#endif /* TASKS_IDLETASK_H_ */
