/*
 * ButtonTask.h
 *
 *  Created on: 03.09.2019
 *      Author: pmale
 */

#ifndef TASKS_BUTTONTASK_H_
#define TASKS_BUTTONTASK_H_

#include "../PLATFORM.h"
#include "../templates/BoomfyTask.h"
#include "BluetoothTask.h"
#include "WifiTask.h"

class ButtonTask : public BoomfyTask{
public:
	typedef enum{
		BUT_BT=32,
		BUT_WIFI=33
	}buttonPin;
	static ButtonTask& getInstance();
	~ButtonTask();
private:
	ButtonTask();
	ButtonTask(const ButtonTask&);
	ButtonTask & operator = (const ButtonTask &);
	void bt_isr();
	void wifi_isr();
	void run();
	static void btisrWrapper(void* _this){((ButtonTask*)_this)->bt_isr();}
	static void wifiisrWrapper(void* _this){((ButtonTask*)_this)->wifi_isr();}

	bool bt_pressed, wifi_pressed;
};


#endif /* TASKS_BUTTONTASK_H_ */
