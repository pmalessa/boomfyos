/*
 * boomfyTask.h
 *
 *  Created on: 03.09.2019
 *      Author: pmale
 */

#ifndef TEMPLATES_BOOMFYTASK_H_
#define TEMPLATES_BOOMFYTASK_H_

#include "../PLATFORM.h"

class BoomfyTask{
public:
	QueueHandle_t queue;
	TaskHandle_t task;
	virtual void run() = 0; //task run function
	static void runWrapper(void* _this){((BoomfyTask*)_this)->run();}
private:
};

#endif /* TEMPLATES_BOOMFYTASK_H_ */
